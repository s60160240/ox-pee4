var rowInd = 0
var colInd = 0
var bew = 'X'
var table = arrayOf(
    arrayOf('0', '1', '2', '3'),
    arrayOf('1', '-', '-', '-'),
    arrayOf('2', '-', '-', '-'),
    arrayOf('3', '-', '-', '-')
)
fun input() {
    while (true) {
        try {
            print("Please input R C: ")
            val input =  readLine()
            println(input)
            val rcList = input?.split(" ")
            if( rcList?.size != 2 ) {
                println("Error: Must input 2 numbers R C (Ex: 1 2)")
                continue
            }
            println("Row ${rcList?.get(0)} col ${rcList?.get(1)}")
            rowInd = rcList[0].toInt()
            colInd = rcList[1].toInt()
            println("Row $rowInd Col $colInd")
            if(table[rowInd][colInd] == 'X' || table[rowInd][colInd] == 'O'){
                println("yoooooooo")
                continue
            }
            table[rowInd][colInd] = bew
            break
        }catch (t: Throwable) {
            println("Error: ${t.message} , Must be numbers R C (Ex: 1 2)")
        }

    }
}

fun printTable(){
    for ( row in table) {
        for (col in row) {
            print("$col ")
        }
        println()
    }
}

fun change() {
    if (bew == 'X'){
        bew = 'O'
    }else{
        bew = 'X'
    }
}

fun checkwin():Boolean {
    if ((table[1][1] == table[2][2] && table[2][2] == table[3][3]) && table[1][1] != '-') {
            return true
        } else if ((table[1][3] == table[2][2] && table[2][2] == table[3][1]) && table[3][1] != '-') {
            return true
        } else if ((table[1][1] == table[1][2] && table[1][2] == table[1][3]) && table[1][3] != '-') {
            return true
        } else if ((table[2][1] == table[2][2] && table[2][2] == table[2][3]) && table[2][1] != '-') {
            return true
        } else if ((table[3][1] == table[3][2] && table[3][2] == table[3][3]) && table[3][3] != '-') {
            return true
        } else if ((table[1][1] == table[2][1] && table[2][1] == table[3][1]) && table[2][1] != '-') {
            return true
        } else if ((table[1][2] == table[2][2] && table[2][2] == table[3][2]) && table[2][2] != '-') {
            return true
        } else if ((table[1][3] == table[3][2] && table[3][2] == table[3][3]) && table[3][2] != '-') {
            return true
        }
    return false
}

fun main() {
    println("Welcome to the XO Game")
    while (true) {
        printTable()
        input()
        if(checkwin() == false){
            print("win " + bew)
            break
        }
        change()
    }


}